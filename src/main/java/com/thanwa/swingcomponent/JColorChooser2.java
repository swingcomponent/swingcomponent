/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanwa.swingcomponent;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JFrame;
import javax.swing.JTextArea;

/**
 *
 * @author tud08
 */
public class JColorChooser2 extends JFrame implements ActionListener {

    JFrame frame;
    JButton btn;
    JTextArea ta;

    JColorChooser2() {
        frame = new JFrame("Color Chooser Example.");
        btn = new JButton("Pad Color");
        btn.setBounds(200, 250, 100, 30);
        ta = new JTextArea();
        ta.setBounds(10, 10, 300, 200);
        btn.addActionListener(this);
        frame.add(btn);
        frame.add(ta);
        frame.setLayout(null);
        frame.setSize(400, 400);
        frame.setVisible(true);

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Color c = JColorChooser.showDialog(this, "Choose", Color.CYAN);
        ta.setBackground(c);
    }
    public static void main(String[] args) {
        new JColorChooser2();
    }
}
