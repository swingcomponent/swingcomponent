/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanwa.swingcomponent;

import javax.swing.*;

/**
 *
 * @author tud08
 */
public class JSpinner1 {

    public static void main(String[] args) {
        JFrame frame = new JFrame("Spinner Example");
        SpinnerModel value = new SpinnerNumberModel(5,  0, 10, 1);  
        JSpinner spinner = new JSpinner(value);
        spinner.setBounds(100, 100, 50, 30);
        frame.add(spinner);
        frame.setSize(300, 300);
        frame.setLayout(null);
        frame.setVisible(true);
    }
}
