/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanwa.swingcomponent;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JRadioButton;

/**
 *
 * @author tud08
 */
public class JRadioButton2 extends JFrame implements ActionListener {

    JRadioButton rbtn1, rbtn2;
    JButton btn;

    JRadioButton2() {
        rbtn1 = new JRadioButton("Male");
        rbtn1.setBounds(100, 50, 100, 30);
        rbtn2 = new JRadioButton("Female");
        rbtn2.setBounds(100, 100, 100, 30);
        ButtonGroup bg = new ButtonGroup();
        bg.add(rbtn1);
        bg.add(rbtn2);
        btn = new JButton("click");
        btn.setBounds(100, 150, 80, 30);
        btn.addActionListener(this);
        add(rbtn1);
        add(rbtn2);
        add(btn);
        setSize(300, 300);
        setLayout(null);
        setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (rbtn1.isSelected()) {
            JOptionPane.showMessageDialog(this, "You are Male.");
        }
        if (rbtn2.isSelected()) {
            JOptionPane.showMessageDialog(this, "You are Female.");
        }
    }
    public static void main(String[] args) {
        new JRadioButton2();
    }
}
