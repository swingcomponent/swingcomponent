/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanwa.swingcomponent;

import java.awt.*;
import javax.swing.JFrame;

/**
 *
 * @author tud08
 */
public class DisplayingImage1 extends Canvas{

    public void paint(Graphics g) {

        Toolkit t = Toolkit.getDefaultToolkit();
        Image i = t.getImage("p3.gif");
        g.drawImage(i, 120, 100, this);

    }

    public static void main(String[] args) {
        DisplayingImage1 m = new DisplayingImage1();
        JFrame frame = new JFrame();
        frame.add(m);
        frame.setSize(400, 400);
        frame.setVisible(true);
    }
}
