/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanwa.swingcomponent;

import javax.swing.JButton;
import javax.swing.JFrame;

/**
 *
 * @author tud08
 */
public class SwingIntroduction3 extends JFrame {

    JFrame frame;
    
    SwingIntroduction3() {
        JButton btn = new JButton("click");
        btn.setBounds(130, 100, 100, 40);
        
        add(btn);//adding button on frame  
        setSize(400,500);  
        setLayout(null);  
        setVisible(true);
    }
    
    public static void main(String[] args) {
        new SwingIntroduction3();
    }
}
