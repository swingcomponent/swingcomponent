/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanwa.swingcomponent;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextField;

/**
 *
 * @author tud08
 */
public class JTextField2 implements ActionListener {

    JTextField txt1, txt2, txt3;

    JButton btn1, btn2;

    JTextField2() {
        JFrame frame = new JFrame();
        txt1 = new JTextField();
        txt1.setBounds(50, 50, 150, 20);
        txt2 = new JTextField();
        txt2.setBounds(50, 100, 150, 20);
        txt3 = new JTextField();
        txt3.setBounds(50, 150, 150, 20);
        txt3.setEditable(false);
        btn1 = new JButton("+");
        btn1.setBounds(50, 200, 50, 50);
        btn2 = new JButton("-");
        btn2.setBounds(120, 200, 50, 50);
        btn1.addActionListener(this);
        btn2.addActionListener(this);
        frame.add(txt1);
        frame.add(txt2);
        frame.add(txt3);
        frame.add(btn1);
        frame.add(btn2);
        frame.setSize(300, 300);
        frame.setLayout(null);
        frame.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String s1=txt1.getText();  
        String s2=txt2.getText();  
        int a=Integer.parseInt(s1);  
        int b=Integer.parseInt(s2);  
        int c=0;  
        if(e.getSource()==btn1){  
            c=a+b;  
        }else if(e.getSource()==btn2){  
            c=a-b;  
        }  
        String result=String.valueOf(c);  
        txt3.setText(result);
    }
    public static void main(String[] args) {
        new JTextField2();
    }
}
