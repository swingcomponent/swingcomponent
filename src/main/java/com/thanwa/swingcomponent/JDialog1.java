/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanwa.swingcomponent;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

/**
 *
 * @author tud08
 */
public class JDialog1 {

    private static JDialog dia;

    JDialog1() {
        JFrame frame = new JFrame();
        dia = new JDialog(frame, "Dialog Example", true);
        dia.setLayout(new FlowLayout());
        JButton b = new JButton("OK");
        b.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JDialog1.dia.setVisible(false);
            }
        });
        dia.add(new JLabel("Click button to continue."));
        dia.add(b);
        dia.setSize(300, 300);
        dia.setVisible(true);
    }

    public static void main(String[] args) {
        new JDialog1();
    }
}
