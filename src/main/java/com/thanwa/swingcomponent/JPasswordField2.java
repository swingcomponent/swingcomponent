/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanwa.swingcomponent;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

/**
 *
 * @author tud08
 */
public class JPasswordField2 {

    public static void main(String[] args) {
        JFrame frame = new JFrame("Password Field");

        final JLabel label = new JLabel();
        label.setBounds(20, 150, 200, 50);

        final JPasswordField value = new JPasswordField();
        value.setBounds(100, 75, 100, 30);

        JLabel label1 = new JLabel("Username:");
        label1.setBounds(20, 20, 80, 30);
        JLabel label2 = new JLabel("Password:");
        label2.setBounds(20, 75, 80, 30);

        JButton btn = new JButton("Login");
        btn.setBounds(100, 120, 80, 30);

        final JTextField text = new JTextField();
        text.setBounds(100, 20, 100, 30);
        frame.add(value);
        frame.add(label1);
        frame.add(label);
        frame.add(label2);
        frame.add(btn);
        frame.add(text);
        frame.setSize(300, 300);
        frame.setLayout(null);
        frame.setVisible(true);
        btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String data = "Username " + text.getText();
                data += ", Password: " + new String(value.getPassword());
                label.setText(data);
            }

        });
    }
}
