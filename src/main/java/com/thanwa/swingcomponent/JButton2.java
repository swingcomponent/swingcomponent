/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanwa.swingcomponent;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextField;

/**
 *
 * @author tud08
 */
public class JButton2 {

    public static void main(String[] args) {
        JFrame frame = new JFrame("Button");

        final JTextField txt = new JTextField();
        txt.setBounds(50, 50, 150, 20);
        JButton btn = new JButton("Click Here");
        btn.setBounds(50, 100, 95, 30);
        btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                txt.setText("Welcome to Javatpoint.");
            }
        });
        frame.add(btn);frame.add(txt);  
        frame.setSize(400,400);  
        frame.setLayout(null);  
        frame.setVisible(true);
    }
}
