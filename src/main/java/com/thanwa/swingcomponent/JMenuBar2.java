/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanwa.swingcomponent;

import javax.swing.*;
import java.awt.event.*;

/**
 *
 * @author tud08
 */
public class JMenuBar2 implements ActionListener {

    JFrame frame;
    JMenuBar mb;
    JMenu file, edit, help;
    JMenuItem cut, copy, paste, selectAll;
    JTextArea ta;

    JMenuBar2() {
        frame = new JFrame();
        cut = new JMenuItem("cut");
        copy = new JMenuItem("copy");
        paste = new JMenuItem("paste");
        selectAll = new JMenuItem("selectAll");
        cut.addActionListener(this);
        copy.addActionListener(this);
        paste.addActionListener(this);
        selectAll.addActionListener(this);
        mb = new JMenuBar();
        file = new JMenu("File");
        edit = new JMenu("Edit");
        help = new JMenu("Help");
        edit.add(cut);
        edit.add(copy);
        edit.add(paste);
        edit.add(selectAll);
        mb.add(file);
        mb.add(edit);
        mb.add(help);
        ta = new JTextArea();
        ta.setBounds(5, 5, 360, 320);
        frame.add(mb);
        frame.add(ta);
        frame.setJMenuBar(mb);
        frame.setLayout(null);
        frame.setSize(400, 400);
        frame.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == cut) {
            ta.cut();
        }
        if (e.getSource() == paste) {
            ta.paste();
        }
        if (e.getSource() == copy) {
            ta.copy();
        }
        if (e.getSource() == selectAll) {
            ta.selectAll();
        }
    }
    public static void main(String[] args) {
        new JMenuBar2();
    }
}
