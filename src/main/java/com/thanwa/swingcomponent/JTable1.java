/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanwa.swingcomponent;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;

/**
 *
 * @author tud08
 */
public class JTable1 {

    JFrame frame;

    JTable1() {
        frame = new JFrame();
        String data[][] = {{"101", "Amit", "670000"}, {"102", "Jai", "780000"}, {"101", "Sachin", "700000"}};
        String column[] = {"ID", "NAME", "SALARY"};
        JTable jtb = new JTable(data, column);
        jtb.setBounds(30, 40, 200, 300);
        JScrollPane jsp = new JScrollPane(jtb);
        frame.add(jsp);
        frame.setSize(300, 400);
        frame.setVisible(true);
    }
    public static void main(String[] args) {
        new JTable1();
    }
}
