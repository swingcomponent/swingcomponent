/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanwa.swingcomponent;

import javax.swing.ButtonGroup;
import javax.swing.JFrame;
import javax.swing.JRadioButton;

/**
 *
 * @author tud08
 */
public class JRadioButton1 {

    JFrame frame;

    JRadioButton1() {
        frame = new JFrame();
        JRadioButton r1 = new JRadioButton("A) Male");
        JRadioButton r2 = new JRadioButton("B) Female");
        r1.setBounds(75, 50, 100, 30);
        r2.setBounds(75, 100, 100, 30);
        ButtonGroup btng = new ButtonGroup();
        btng.add(r1);
        btng.add(r2);
        frame.add(r1);
        frame.add(r2);
        frame.setSize(300, 300);
        frame.setLayout(null);
        frame.setVisible(true);
    }public static void main(String[] args) {
        new JRadioButton1();
    }
}
