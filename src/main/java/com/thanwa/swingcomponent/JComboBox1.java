/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanwa.swingcomponent;

import javax.swing.JComboBox;
import javax.swing.JFrame;

/**
 *
 * @author tud08
 */
public class JComboBox1 {

    JFrame frame;

    JComboBox1() {
        frame = new JFrame("ComboBox");
        String country[] = {"India", "Aus", "U.S.A", "England", "Newzealand"};
        JComboBox cb = new JComboBox(country);
        cb.setBounds(50, 50, 90, 20);
        frame.add(cb);
        frame.setLayout(null);
        frame.setSize(400, 500);
        frame.setVisible(true);
    }
    public static void main(String[] args) {
        new JComboBox1();
    }
}
